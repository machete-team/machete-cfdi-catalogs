const hourType = [
  { key: '01', value: 'Dobles' },
  { key: '02', value: 'Triples' },
  { key: '03', value: 'Simples' },
];

module.exports = hourType;
