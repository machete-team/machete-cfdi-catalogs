const payMethod = [
  { key: 'PUE', value: 'Pago en una sola exhibición' },
  { key: 'PPD', value: 'Pago en parcialidades o diferido' },
];

module.exports = payMethod;
