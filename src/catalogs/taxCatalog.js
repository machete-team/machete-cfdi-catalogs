const tax = [
  { key: '001', value: 'ISR' },
  { key: '002', value: 'IVA' },
  { key: '003', value: 'IEPS' },
];

module.exports = tax;
