const contractType = [
  { key: '01', value: 'Contrato de trabajo por tiempo indeterminado' },
  { key: '02', value: 'Contrato de trabajo para obra determinada' },
  { key: '03', value: 'Contrato de trabajo por tiempo determinado' },
  { key: '04', value: 'Contrato de trabajo por temporada' },
  { key: '05', value: 'Contrato de trabajo sujeto a prueba' },
  { key: '06', value: 'Contrato de trabajo con capacitación inicial' },
  { key: '07', value: 'Modalidad de contratación por pago de hora laborada' },
  { key: '08', value: 'Modalidad de trabajo por comisión laboral' },
  { key: '09', value: 'Modalidades de contratación donde no existe relación de trabajo' },
  { key: '10', value: 'Jubilación, pensión, retiro.' },
  { key: '99', value: 'Otro contrato' },
];

module.exports = contractType;
