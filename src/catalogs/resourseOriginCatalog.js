const resourceOrigin = [
  { key: 'IP', value: 'Ingresos propios.' },
  { key: 'IF', value: 'Ingreso federales.' },
  { key: 'IM', value: 'Ingresos mixtos.' },
];

module.exports = resourceOrigin;
