const payrollType = [
  { key: 'O', value: 'Nómina ordinaria' },
  { key: 'E', value: 'Nómina extraordinaria' },
];

module.exports = payrollType;
