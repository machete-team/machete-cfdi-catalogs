const relationType = [
  { key: '01', value: 'Nota de crédito de los documentos relacionados' },
  { key: '02', value: 'Nota de débito de los documentos relacionados' },
  { key: '03', value: 'Devolución de mercancía sobre facturas o traslados previos' },
  { key: '04', value: 'Sustitución de los CFDI previos' },
  { key: '05', value: 'Traslados de mercancias facturados previamente' },
  { key: '06', value: 'Factura generada por los traslados previos' },
  { key: '07', value: 'CFDI por aplicación de anticipo' },
  { key: '08', value: 'Factura generada por pagos en parcialidades' },
  { key: '09', value: 'Factura generada por pagos diferidos' },
];

module.exports = relationType;
