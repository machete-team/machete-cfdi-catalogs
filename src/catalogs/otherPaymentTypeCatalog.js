const otherPaymentType = [
  { key: '001', value: 'Reintegro de ISR pagado en exceso (siempre que no haya sido enterado al SAT).' },
  { key: '002', value: 'Subsidio para el empleo (efectivamente entregado al trabajador).' },
  { key: '003', value: 'Viáticos (entregados al trabajador).' },
  { key: '004', value: 'Aplicación de saldo a favor por compensación anual.' },
  { key: '005', value: 'Reintegro de ISR retenido en exceso de ejercicio anterior (siempre que no haya sido enterado al SAT).' },
  { key: '999', value: 'Pagos distintos a los listados y que no deben considerarse como ingreso por sueldos, salarios o ingresos asimilados.' },

];

module.exports = otherPaymentType;
