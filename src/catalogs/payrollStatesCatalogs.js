const payrollStates = [
  { key: 'AGU', value: 'Aguascalientes' },
  { key: 'BCN', value: 'Baja California' },
  { key: 'BCS', value: 'Baja California Sur' },
  { key: 'CAM', value: 'Campeche' },
  { key: 'CHP', value: 'Chiapas' },
  { key: 'CHH', value: 'Chihuahua' },
  { key: 'COA', value: 'Coahuila' },
  { key: 'COL', value: 'Colima' },
  { key: 'DIF', value: 'Ciudad de México' },
  { key: 'DUR', value: 'Durango' },
  { key: 'GUA', value: 'Guanajuato' },
  { key: 'GRO', value: 'Guerrero' },
  { key: 'HID', value: 'Hidalgo' },
  { key: 'JAL', value: 'Jalisco' },
  { key: 'MEX', value: 'Estado de México' },
  { key: 'MIC', value: 'Michoacán' },
  { key: 'MOR', value: 'Morelos' },
  { key: 'NAY', value: 'Nayarit' },
  { key: 'NLE', value: 'Nuevo León' },
  { key: 'OAX', value: 'Oaxaca' },
  { key: 'PUE', value: 'Puebla' },
  { key: 'QUE', value: 'Querétaro' },
  { key: 'ROO', value: 'Quintana Roo' },
  { key: 'SLP', value: 'San Luis Potosí' },
  { key: 'SIN', value: 'Sinaloa' },
  { key: 'SON', value: 'Sonora' },
  { key: 'TAB', value: 'Tabasco' },
  { key: 'TAM', value: 'Tamaulipas' },
  { key: 'TLA', value: 'Tlaxcala' },
  { key: 'VER', value: 'Veracruz' },
  { key: 'YUC', value: 'Yucatán' },
  { key: 'ZAC', value: 'Zacatecas' },
];

module.exports = payrollStates;

































