const voucherType = [
  { key: 'I', value: 'Ingreso' },
  { key: 'E', value: 'Egreso' },
  { key: 'T', value: 'Traslado' },
  { key: 'N', value: 'Nómina' },
  { key: 'P', value: 'Pago' },
];

module.exports = voucherType;
