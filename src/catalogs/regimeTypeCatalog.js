const regimeType = [
  { key: '02', value: 'Sueldos' },
  { key: '03', value: 'Jubilados' },
  { key: '04', value: 'Pensionados' },
  { key: '05', value: 'Asimilados Miembros Sociedades Cooperativas Produccion' },
  { key: '06', value: 'Asimilados Integrantes Sociedades Asociaciones Civiles' },
  { key: '07', value: 'Asimilados Miembros consejos' },
  { key: '08', value: 'Asimilados comisionistas' },
  { key: '09', value: 'Asimilados Honorarios' },
  { key: '10', value: 'Asimilados acciones' },
  { key: '11', value: 'Asimilados otros' },
  { key: '12', value: 'Jubilados o Pensionados' },
  { key: '13', value: 'Indemnización o Separación' },
  { key: '99', value: 'Otro Regimen' },
];

module.exports = regimeType;
