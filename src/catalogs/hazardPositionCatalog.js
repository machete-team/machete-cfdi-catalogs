const workingDayType = [
  { key: 1, value: 'Clase I' },
  { key: 2, value: 'Clase II' },
  { key: 3, value: 'Clase III' },
  { key: 4, value: 'Clase IV' },
  { key: 5, value: 'Clase V' },
  { key: 99, value: 'No aplica' },

];

module.exports = workingDayType;
