const workingDayType = [
  { key: '01', value: 'Diurna' },
  { key: '02', value: 'Nocturna' },
  { key: '03', value: 'Mixta' },
  { key: '04', value: 'Por hora' },
  { key: '05', value: 'Reducida' },
  { key: '06', value: 'Continuada' },
  { key: '07', value: 'Partida' },
  { key: '08', value: 'Por turnos' },
  { key: '99', value: 'Otra Jornada' },
];

module.exports = workingDayType;
