const cfdiUse = [
  { key: 'G01', value: 'Adquisición de mercancias' },
  { key: 'G02', value: 'Devoluciones, descuentos o bonificaciones' },
  { key: 'G03', value: 'Gastos en general' },
  { key: 'I01', value: 'Construcciones' },
  { key: 'I02', value: 'Mobilario y equipo de oficina por inversiones' },
  { key: 'I03', value: 'Equipo de transporte' },
  { key: 'I04', value: 'Equipo de computo y accesorios' },
  { key: 'I05', value: 'Dados, troqueles, moldes, matrices y herramental' },
  { key: 'I06', value: 'Comunicaciones telefónicas' },
  { key: 'I07', value: 'Comunicaciones satelitales' },
  { key: 'I08', value: 'Otra maquinaria y equipo' },
  { key: 'D01', value: 'Honorarios médicos, dentales y gastos hospitalarios.' },
  { key: 'D02', value: 'Gastos médicos por incapacidad o discapacidad' },
  { key: 'D03', value: 'Gastos funerales.' },
  { key: 'D04', value: 'Donativos.' },
  { key: 'D05', value: 'Intereses reales efectivamente pagados por créditos hipotecarios (casa habitación).' },
  { key: 'D06', value: 'Aportaciones voluntarias al SAR.' },
  { key: 'D07', value: 'Primas por seguros de gastos médicos.' },
  { key: 'D08', value: 'Gastos de transportación escolar obligatoria.' },
  { key: 'D09', value: 'Depósitos en cuentas para el ahorro, primas que tengan como base planes de pensiones.' },
  { key: 'D10', value: 'Pagos por servicios educativos (colegiaturas)' },
  { key: 'P01', value: 'Por definir' },

];

module.exports = cfdiUse;
