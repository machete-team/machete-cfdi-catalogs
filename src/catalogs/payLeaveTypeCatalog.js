const payLeaveType = [
  { key: '01', value: 'Riesgo de trabajo.' },
  { key: '02', value: 'Enfermedad en general.' },
  { key: '03', value: 'Maternidad.' },
];

module.exports = payLeaveType;
