const paymentPeriodicity = [
  { key: '01', value: 'Diario' },
  { key: '02', value: 'Semanal' },
  { key: '03', value: 'Catorcenal' },
  { key: '04', value: 'Quincenal' },
  { key: '05', value: 'Mensual' },
  { key: '06', value: 'Bimestral' },
  { key: '07', value: 'Unidad obra' },
  { key: '08', value: 'Comisión' },
  { key: '09', value: 'Precio alzado' },
  { key: '10', value: 'Decenal' },
  { key: '99', value: 'Otra Periodicidad' },
];

module.exports = paymentPeriodicity;
