const cfdiUse = require('./catalogs/CFDIUseCatalog');
const country = require('./catalogs/countryCatalog');
const currency = require('./catalogs/currencyCatalog');
const keyUnit = require('./catalogs/keyUnitCatalog');
const payMethod = require('./catalogs/payMethodCatalog');
const relationType = require('./catalogs/relationTypeCatalog');
const tax = require('./catalogs/taxCatalog');
const taxRegime = require('./catalogs/taxRegimeCatalog');
const voucherType = require('./catalogs/voucherTyeCatalog');
const wayTopay = require('./catalogs/wayToPayCatalog');
const bank = require('./catalogs/bankCatalog');
const regimeType = require('./catalogs/regimeTypeCatalog');
const perceptType = require('./catalogs/perceptTypeCatalog');
const otherPaymentType = require('./catalogs/otherPaymentTypeCatalog');
const payrollType = require('./catalogs/payrollTypeCatalog');
const workingDayType = require('./catalogs/workingDayTypeCatalog');
const payLeaveType = require('./catalogs/payLeaveTypeCatalog');
const hourType = require('./catalogs/hourTypeCatalog');
const deductionType = require('./catalogs/deductionTypeCatalog');
const contractType = require('./catalogs/contractTypeCatalog');
const paymentPeriodicity = require('./catalogs/paymentPeriodicityCatalog');
const resourceOrigin = require('./catalogs/resourseOriginCatalog');
const stateCountry = require('./catalogs/stateCountryCatalog');
const hazardPosition = require('./catalogs/hazardPositionCatalog');
const payrollStates = require('./catalogs/payrollStatesCatalogs');

module.exports = {
  cfdiUse,
  country,
  currency,
  keyUnit,
  payMethod,
  relationType,
  tax,
  taxRegime,
  voucherType,
  wayTopay,
  bank,
  regimeType,
  perceptType,
  otherPaymentType,
  payrollType,
  workingDayType,
  payLeaveType,
  hourType,
  deductionType,
  contractType,
  paymentPeriodicity,
  resourceOrigin,
  stateCountry,
  hazardPosition,
  payrollStates,
};
